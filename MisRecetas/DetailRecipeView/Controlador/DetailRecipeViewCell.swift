//
//  DetailRecipeViewCell.swift
//  MisRecetas
//
//  Created by Josep Elias on 08/01/2019.
//  Copyright © 2019 Josep Elias. All rights reserved.
//

import UIKit

class DetailRecipeViewCell: UITableViewCell {
    // labels de la cela
    @IBOutlet var keyLabel: UILabel!
    @IBOutlet var valueLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
