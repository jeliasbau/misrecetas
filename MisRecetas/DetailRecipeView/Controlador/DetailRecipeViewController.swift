//
//  DetailRecipeViewController.swift
//  MisRecetas
//
//  Created by Josep Elias on 13/11/18.
//  Copyright © 2018 Josep Elias. All rights reserved.
//

import UIKit

class DetailRecipeViewController: UIViewController {
    // objectes de la vista
    @IBOutlet var detailImageView: UIImageView!
    @IBOutlet var detailTableView: UITableView!
    @IBOutlet var ratingButton: UIButton!
    
    // variable recepta de tipo "Recipe" per poder assignar tots els valors de
    // les propietats a aquesta vista (info pass)
    var recipe: Recipe!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // configureUI
        configureUI()
        
        // assignem la imatge de la recepta a l'imageView
        self.detailImageView.image = self.recipe.image
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // mostrar i amagar la navegation bar al fer scroll o swipe
        navigationController?.hidesBarsOnSwipe = true
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    func configureUI() {
        // Navegation title
        navigationItem.title = recipe.name
        // cambiar color de la taula
        self.detailTableView.backgroundColor = UIColor(red: 0.85, green: 0.85, blue: 0.85, alpha: 0.25)
        // eliminar celes en cas que en sobren
        self.detailTableView.tableFooterView = UIView(frame: .zero)
        // canviar el color del separador de la llista
        self.detailTableView.separatorColor = UIColor(red: 0.85, green: 0.85, blue: 0.85, alpha: 0.65)
        // celes ajustables
        self.detailTableView.estimatedRowHeight = UITableView.automaticDimension
        self.detailTableView.rowHeight = UITableView.automaticDimension
    
    }
    
    @IBAction func close(segue: UIStoryboardSegue) {
        
        if let reviewVC = segue.source as? ReviewViewController {
            if let rating = reviewVC.ratingSelected {
                let image = UIImage(named: rating)
                self.ratingButton.setImage(image, for: .normal)
            }
        }
            
        
    }

}

extension DetailRecipeViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3  // utilitzem 3 seccions per afegir tota la info de la recepta
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // utilitzem un switch per assignar a cada seccio el numero de files en funcio dels valors de la recepta
        switch section {
            case 0:
                return 3
            case 1:
                return self.recipe.ingredients.count // tantes files com ingredients tingui
            case 2:
                return self.recipe.steps.count  // tantes files como passos tingui
        default:
            return 0 // si cap dels 3 casos anteriors es compleix retornara 0 files
        }

    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // instanciem la DetailCell
        let detailCell = self.detailTableView.dequeueReusableCell(withIdentifier: "DetailCell", for: indexPath) as! DetailRecipeViewCell
        
        // canviem el color de fons de les celes
        detailCell.backgroundColor = UIColor.clear
        
        /*passarem la informacio de cada recepta a la taula de forma mes detallada fem un switch case (on hi haura seccions i  a cada seccio les files seran una propietat de la recepta i el seu valor)*/
        switch indexPath.section {
        case 0:
            switch indexPath.row {
            case 0:
                    detailCell.keyLabel.text = "Nombre: "
                    detailCell.valueLabel.text = "\(self.recipe.name!)"
            case 1:
                    detailCell.keyLabel.text = "Tiempo: "
                    detailCell.valueLabel.text = "\(self.recipe.time!) min"
            case 2:
                    detailCell.keyLabel.text = "Favorita: "
                    if self.recipe.isFavourite == false {
                        detailCell.valueLabel.text = "No"
                    }
            default: break
            }
        case 1:
            if indexPath.row == 0 {
                detailCell.keyLabel.text = "Ingredientes: "
            } else {
                detailCell.keyLabel.text = ""
            }
            detailCell.valueLabel.text = self.recipe.ingredients[indexPath.row]
        case 2:
            if indexPath.row == 0 {
                detailCell.keyLabel.text = "Pasos: "
            } else {
                detailCell.keyLabel.text = ""
            }
            detailCell.valueLabel.text = self.recipe.steps[indexPath.row]
        
        default: break
        }
        
        return detailCell
    }
    
}

extension DetailRecipeViewController: UITableViewDelegate {
    
}

