//
//  RecipeCellTableViewCell.swift
//  MisRecetas
//
//  Created by Josep Elias on 13/11/18.
//  Copyright © 2018 Josep Elias. All rights reserved.
//

import UIKit

class RecipeCell: UITableViewCell {

    // objectes de la cel.la recipeCell
    @IBOutlet var recipeImage: UIImageView!
    @IBOutlet var recipeNameLabel: UILabel!
    @IBOutlet var recipeTimeLabel: UILabel!
    @IBOutlet var recipeIngredientsLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
