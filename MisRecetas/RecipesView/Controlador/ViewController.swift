//
//  ViewController.swift
//  MisRecetas
//
//  Created by Josep Elias on 12/11/18.
//  Copyright © 2018 Josep Elias. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    // objectes de la vista
    @IBOutlet var recipeTableView: UITableView!
    // array de receptes
    var recipesArray : [Recipe] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // configureUI
        configureUI()
        
        // receptes de l'array (hauria de provar d'una altra manera mes ordenada)
        var recipe = Recipe(name: "Tortilla de patatas", image: #imageLiteral(resourceName: "TortillaPatatas"), ingredients: ["ous", "patates"], steps: ["fregir", "pelar patates"], time: 20)
        recipesArray.append(recipe)
        
        recipe = Recipe(name: "Paella", image: #imageLiteral(resourceName: "Paella"), ingredients: ["arros","caldo de peix","primento","llangostins","ceba","calamars"], steps: ["preparar sofregit","afegir caldo", "tirar arros"], time: 60)
        recipesArray.append(recipe)
        
        recipe = Recipe(name: "Macarrones", image: #imageLiteral(resourceName: "Macarrones"), ingredients: ["Pasta","Tomate","Cebolla","Carne picada","Perejil"], steps: ["Freir la cebolla y la carne picada","Añadir el tomate al sofrito","Hervir la pasta","Juntar todo"], time: 25)
        recipesArray.append(recipe)
        
        recipe = Recipe(name: "Torrijas", image: #imageLiteral(resourceName: "Torrijas"), ingredients: ["Pan","Huevos","Leche","Azucar","Canela"], steps: ["Cortar pan","Sazonar con leche y luego con huevo","Freir el pan","Rebozar con azucar y canela"], time: 15)
        recipesArray.append(recipe)
        
        recipe = Recipe(name: "Entrecot", image: #imageLiteral(resourceName: "Entrecot"), ingredients: ["Entrecot","Aceite","Sal gorda","Pimienta"], steps: ["Calentar la sarten y dar un par de vueltas al entrecot","Poner sal gorda y perejil"], time: 5)
        recipesArray.append(recipe)
        
        recipe = Recipe(name: "Salmon", image: #imageLiteral(resourceName: "Salmon"), ingredients: ["Salmon","Sal gorda","Perejil"], steps: ["Calentar la sarten y dejar al salmon hacerse bien por cada lado","Poner sal gorda y perejil"], time: 10)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // amaga la navegation bar
        navigationController?.hidesBarsOnSwipe = true
    }
    
    func configureUI() {
        // NavegationButton
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
}

extension ViewController : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return recipesArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // instanciem la cela per personalitzarla i assignem valors a les labels per mostrarlos a la vista
        let recipe = recipesArray[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "recipeCell", for: indexPath) as! RecipeCell
        
            cell.recipeImage.image = recipe.image
            cell.recipeNameLabel.text = recipe.name
            cell.recipeIngredientsLabel.text = "\(recipe.ingredients.count) ingredientes"
            cell.recipeTimeLabel.text = "\(recipe.time!) min"
        
            return cell
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        // compartir a xarxes socials
        let shareAction = UITableViewRowAction(style: .default, title: "Compartir") { (action, indexPath) in
            let shareDefaultText = "(Estoy mirando la receta de \(self.recipesArray[indexPath.row].name)"
            let image = self.recipesArray[indexPath.row].image
            let activityController = UIActivityViewController(activityItems:[shareDefaultText, image!], applicationActivities: nil)
            self.present(activityController, animated: true, completion: nil)
        }
        // color del shareAction
        shareAction.backgroundColor = UIColor(red: 30.0/255.0, green: 164.0/255.0, blue: 253.0/255.0, alpha: 1.0)
        
        // borrar de la cela (swipe)
        let deleteAction = UITableViewRowAction(style: .destructive, title: "Borrar") { (action, indexPath) in
            self.recipesArray.remove(at: indexPath.row)
            self.recipeTableView.deleteRows(at: [indexPath], with: .fade)
        }
        // color del deleteAction
        deleteAction.backgroundColor = UIColor(red: 202.0/255.0, green: 202.0/255.0, blue: 202.0/255.0, alpha: 1.0)
        
        return [shareAction, deleteAction]
        
    }
    
}

extension ViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
       // preparem un segue per transferir info al DetailViewController
        if segue.identifier == "showDetail" {
            if let indexpath = self.recipeTableView.indexPathForSelectedRow {
                let selectedRecipe = self.recipesArray[indexpath.row]
                let detailVC = segue.destination as! DetailRecipeViewController
                detailVC.recipe = selectedRecipe
            }
        }
    }
}



        

                





    

