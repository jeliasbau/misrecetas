//
//  Recipe.swift
//  MisRecetas
//
//  Created by Josep Elias on 12/11/18.
//  Copyright © 2018 Josep Elias. All rights reserved.
//

import UIKit

class Recipe: NSObject {
    // propietats
    var name : String!
    var image : UIImage!
    var ingredients : [String]!
    var steps : [String]!
    var time : Int!
    var isFavourite: Bool! = false
    
    // constructor
    init(name : String, image : UIImage, ingredients : [String], steps : [String], time : Int) {
        self.name = name
        self.image = image
        self.ingredients = ingredients
        self.steps = steps
        self.time = time
        
    }
}
