//
//  ReviewViewController.swift
//  MisRecetas
//
//  Created by Josep Elias on 17/01/2019.
//  Copyright © 2019 Josep Elias. All rights reserved.
//

import UIKit

class ReviewViewController: UIViewController {

    // elements de la vista
    @IBOutlet var backgroundImageView: UIImageView!
    
    // variables
    var ratingSelected: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureUI()
    }
    
    // configuració de la vista
    func configureUI() {
        // per difuminar la imatge
        let blurEffect = UIBlurEffect(style: .dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        backgroundImageView.addSubview(blurEffectView)
    }
    
    @IBAction func ratingPressed(_ sender: UIButton) {
        // switch per assignar un valor(string) a cadascun del botons
        switch sender.tag {
        case 1:
            ratingSelected = "dislike"
        case 2:
            ratingSelected = "good"
        case 3:
            ratingSelected = "great"
        default:
            break
        }
        // segue per tornar al DetailView
        performSegue(withIdentifier: "unwindToDetailView", sender: sender)
    }
    
}
